import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom'; 
import { useContext } from 'react';
import UserContext from '../UserContext.js';

export default function AppNavbar(){
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="dark" expand="lg" className="play-font custom-navbar" style={{ color: '#ffffff' }}>
      <Container fluid>
          <Navbar.Brand style={{ color: '#ffffff' }} as={Link} to='/'>GAME RON</Navbar.Brand>
          
          <Navbar.Toggle aria-controls="basic-navbar-nav"/>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
              <Nav.Link style={{ color: '#ffffff' }} as={NavLink} to='/'>Home</Nav.Link>
            <Nav.Link style={{ color: '#ffffff' }} as={NavLink} to='/products/'>Products</Nav.Link>

              { (user.id !== null) ?
                  user.isAdmin ?
                    <>
                      <Nav.Link style={{ color: '#ffffff' }} as={NavLink} to='/products/add'>Add Products</Nav.Link>
                      <Nav.Link style={{ color: '#ffffff' }} as={NavLink} to='/logout'>Logout</Nav.Link>
                    </>
                  :
                    <>
                      <Nav.Link style={{ color: '#ffffff' }} as={NavLink} to='/logout'>Logout</Nav.Link>
                    </>
                : 
                  <>
                    <Nav.Link style={{ color: '#ffffff' }} as={NavLink} to='/register'>Register</Nav.Link>
                    <Nav.Link style={{ color: '#ffffff' }} as={NavLink} to='/login'>Login</Nav.Link>
                  </>   
              }
            </Nav>
          </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}
