import React from 'react';
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner() {
  return (
    <Row>
      <Col className="p-5 text-center">
        <div className="press-start-font">
          <h1 style={{ color: '#ffffff' }}>GAME RON</h1>
          <p style={{ color: '#ffffff' }}>Leveling Up Your Gaming Lifestyle!</p>
          <Button as={Link} to="/products" variant="primary">
            Explore Games
          </Button>
        </div>
      </Col>
    </Row>
  );
}

