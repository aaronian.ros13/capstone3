import { useState, useEffect } from 'react';
import ProductCard from './ProductCard.js';

export default function UserView({ productsData }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    if (productsData && productsData.length > 0) {
      // Filters out the courses to only be courses that are active
      const active_products = productsData.map(product => {
        if (product.isActive === true) {
          return (
            <ProductCard product={product} key={product._id} />
          );
        } else {
          return null;
        }
      });

      // Set the courses state to the active courses array
      setProducts(active_products);
    }
  }, [productsData]);

  return (
    <>
      <h1 className="text-center play-font" style={{ color: 'white' }}>GAMES</h1>
      {products}
    </>
  );
}
