import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
  return (
    <Row className="my-3">
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Img variant="top" src="./Pictures/Zelda.jpg" alt="Highlight 1" />
          <Card.Body>
            <Card.Title className="adlam-font"> {/* Apply the "ADLaM Display" font */}
              <h2>The Legend of Zelda: Tears Of The Kingdom</h2>
            </Card.Title>
            <Card.Text className="adlam-font"> {/* Apply the "ADLaM Display" font */}
              An epic adventure awaits in the Legend of Zelda: Tears of the Kingdom game, only on the Nintendo Switch system. In this sequel to the Legend of Zelda: Breath of the Wild game, you'll decide your own path through the sprawling landscapes of Hyrule and the mysterious islands floating in the vast skies above.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Img variant="top" src="./Pictures/Eldenring.png" alt="Highlight 2" />
          <Card.Body>
            <Card.Title className="adlam-font"> {/* Apply the "ADLaM Display" font */}
              <h2>Elden Ring</h2>
            </Card.Title>
            <Card.Text className="adlam-font"> {/* Apply the "ADLaM Display" font */}
              Developed by FromSoftware in collaboration with George R. R. Martin, "Elden Ring" is an action RPG set in a dark fantasy world. Players can explore an open world, engage in intense combat, and uncover a rich and mysterious lore.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Img variant="top" src="./Pictures/God.jpg" alt="Highlight 3" />
          <Card.Body>
            <Card.Title className="adlam-font"> {/* Apply the "ADLaM Display" font */}
              <h2>God of War: Ragnarok</h2>
            </Card.Title>
            <Card.Text className="adlam-font"> {/* Apply the "ADLaM Display" font */}
              The next installment in the "God of War" series, "God of War: Ragnarok" continues the story of Kratos and his son Atreus. Set in Norse mythology, the game is expected to feature intense combat, epic battles, and a compelling narrative.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
