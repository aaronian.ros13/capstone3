import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';

export default function AddProduct(){
	// Initialization
	const navigate = useNavigate();
	const {user} = useContext(UserContext);

	// States
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")

	// Functions
	function createProduct(event){
		// Prevents the default behavior of page reload when submitting a form
		event.preventDefault();

		let token = localStorage.getItem('token');

		// Fetch function to the course creation API
		fetch(`${process.env.REACT_APP_API_URL}/api/products/add`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result){
				Swal.fire({
					title: 'New Product Added',
					icon: 'success'
				})

				setName('')
				setDescription('')
				setPrice(0)
				navigate('/products')
			} else {
				Swal.fire({
					title: 'Something went wrong',
					text: 'Product creation unsuccessful',
					icon: 'error'
				})
			}
		})
	}

	return (
		(user.isAdmin == true) ? 
			<>
				<h1 style={{ color: '#ffffff' }} className="play-font my-5 text-center">Add Products</h1>
                <Form onSubmit={event => createProduct(event)}>
                    <Form.Group>
                        <Form.Label style={{ color: '#ffffff' }} className="play-font">Name:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Name" required value={name} onChange={event => {setName(event.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label style={{ color: '#ffffff' }} className="play-font">Description:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Description" required value={description} onChange={event => {setDescription(event.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label style={{ color: '#ffffff' }} className="play-font">Price:</Form.Label>
                        <Form.Control type="number" placeholder="Enter Price" required value={price} onChange={event => {setPrice(event.target.value)}}/>
                    </Form.Group>
                    <Button variant="primary" type="submit" style={{ color: '#ffffff' }} className="play-font my-3">Submit</Button>
                </Form>
			</>
		:
			<Navigate to='/products/all'/>
	)
}