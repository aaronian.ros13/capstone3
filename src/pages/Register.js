import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {
  const { user } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [isAdmin, setIsAdmin] = useState(false);

  function registerUser(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/api/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(response => response.json())
    .then(result => {
      if (result) {
        setEmail("");
        setPassword("");
        setConfirmPassword("");

        Swal.fire({
          title: 'Register successful!',
          text: result.message, // Remove quotes around result.message
          icon: 'success',
          confirmButtonText: 'Cool'
        });
      } else {
        Swal.fire({
          title: 'Something went wrong.',
          text: 'Please try again.',
          icon: 'error',
          confirmButtonText: 'Cool'
        });
      }
    })
    .catch(error => {
      console.error("Error fetching data:", error);
      // Handle the error gracefully, show an error message to the user, etc.
    });
  }

  useEffect(() => {
    if (email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword) {
      setIsAdmin(true);
    } else {
      setIsAdmin(false);
    }
  }, [email, password, confirmPassword]);

  return (
    (user.id !== null) ? 
      <Navigate to='/courses'/>
    :
      <Form onSubmit={registerUser}>
        <h1 className="my-5 text-center" style={{ color: 'white' }}>Register</h1>
        <Form.Group style={{ color: 'white' }} >
          <Form.Label >Email:</Form.Label>
          <Form.Control 
            type="email" 
            placeholder="Enter Email" 
            required
            value={email}
            onChange={event => setEmail(event.target.value)}
          />
        </Form.Group>
        <Form.Group style={{ color: 'white' }}>
          <Form.Label>Password:</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Enter Password" 
            required
            value={password}
            onChange={event => setPassword(event.target.value)}
          />
        </Form.Group>
        <Form.Group style={{ color: 'white' }}>
          <Form.Label>Confirm Password:</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Confirm Password" 
            required
            value={confirmPassword}
            onChange={event => setConfirmPassword(event.target.value)}
          />
        </Form.Group>

        <Button 
        variant="primary" 
        type="submit" 
        disabled={!isAdmin}
        style={{ fontFamily: 'Play, sans-serif', backgroundColor: isAdmin ? '#007bff' : '#ccc', borderColor: '#007bff', marginTop: '20px' }}>
        Submit
        </Button>
      </Form>
  );
}
