import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function ProductItem() {
    const { productId } = useParams();

    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState("");

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
            .then(response => response.json())
            .then(result => {
                setName(result.name);
                setDescription(result.description);
                setPrice(result.price);
            })
            .catch(error => {
                console.error('Fetching product error:', error);
            });
    }, [productId]);

    const orderProduct = () => {
        if (user.isAdmin) {
            Swal.fire({
                title: 'Admins cannot order',
                text: 'Only non-admin accounts can place orders.',
                icon: 'error'
            });
        } else {
            fetch(`${process.env.REACT_APP_API_URL}/api/orders/create`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    products: [
                        { productId: productId, quantity: 1 } // Modify quantity as needed
                    ]
                })
            })
            .then(response => response.json())
            .then(result => {
                if (result.message === 'Order created successfully!') {
                    Swal.fire({
                        title: 'Successfully Ordered!',
                        text: result.message,
                        icon: 'success'
                    });

                    navigate('/products');
                } else {
                    Swal.fire({
                        title: 'Something went wrong',
                        text: 'Please try again.',
                        icon: 'error'
                    });
                }
            })
            .catch(error => {
                console.error('Ordering error:', error);
                Swal.fire({
                    title: 'Error',
                    text: 'An error occurred during ordering.',
                    icon: 'error'
                });
            });
        }
    }

    return (
        <Container className="mt-5">
            <Row>
                <Col>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title><h1>{name}</h1></Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>
                            {user.id !== null ? (
                                <Button variant="primary" onClick={orderProduct}>Order</Button>
                            ) : (
                                <Link className="btn btn-danger btn-block" to="/login">Log In to Order</Link>
                            )}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}
