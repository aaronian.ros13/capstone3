import React from 'react';

function NotFound() {
  return (
    <div style={{ color: 'white' }} >
      <h1 >Not Found</h1>
      <p>The page you visited could not be found.</p>
    </div>
  );
}

export default NotFound;
